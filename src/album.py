#!/usr/bin/python

# album.py
# script to generate photo albums for http://www.axhixh.com.np
# this script is hard coded to generate albums for one website for
# the year 2010; will extract hard coded values over time into
# a configuration file
#
# 1. create a folder for album; folder name is expected to be the path
#       http://www.axhixh.com.np/2010/album_folder/
# 2. copy the script and the css file
# 3. create a imgs folder containing the photos
# 4. create a thumbnails folder with the thumbnails; the script doesn't create them
# 5. run the script to create necessary html files
# 6. copy the folder to web server; update necessary index files on web server
#
# This code is copyright 2010 Ashish Shrestha and distributed under GPL 3
import os
import sys
import urllib
import config

__created__ = '20070312'

album_name = "album"
image_per_page = 20 
album_folder = "album"

if len(sys.argv) != 4:
    album_name = raw_input('album name? ')
    album_folder = raw_input('album folder? ')
    image_per_page = int(raw_input('images per page? '))
else:
    album_name = sys.argv[1]
    album_folder = sys.argv[2] 
    image_per_page = int(sys.argv[3])

index_title = album_name
page_title = album_name
album_url = config.URL + '/2011/' + album_folder + '/'

def encode(url):
    return urllib.quote(url, '')

def is_image(filename):
    return filename.lower()[-len(".jpg"):] == ".jpg"

files =[f for f in os.listdir("imgs") if is_image(f)]
files.sort()

def load_file(filename):
    f = open(filename, 'r')
    w = f.readlines()
    f.close()
    return ''.join(w)

html = load_file('index.tmpl')

def index_to_filename(index):
    if index == 0:
        return "index.html"
    return "index" + str(index) + ".html"

def thumbnail_html(index):
    f = files[index]
    p = f[:-len(".jpg")] + ".html"
    return "<div class='thumbnail'><a href='" + p + "'><img src='thumbnails/" + f + "' alt='" + f + "' title='" + f + "' /></a></div>"

page_html = load_file('page.tmpl')

def get_previous(index):
    if index == 0:
        return ""
    line = """<a rel="prev" href='%(link)s' title='%(name)s'><img alt='Previous' src='thumbnails/%(name)s' /></a>
<a rel="prev" href='%(link)s' title='%(name)s'>Previous</a>"""
    context = {}
    f = files[index - 1]
    context["name"] = f
    context["link"] = f[:-len(".jpg")] + ".html"
    return line % context

def get_next(index):
    if index == len(files) - 1:
        return ""
    line = """<a rel="next" href='%(link)s' title='%(name)s'>Next</a>
<a rel="next" href='%(link)s' title='%(name)s'><img alt='Next' src='thumbnails/%(name)s' /></a>"""
    context = {}
    f = files[index + 1]
    context["name"] = f
    context["link"] = f[:-len(".jpg")] + ".html"
    return line % context

def get_next_img(index):
    if index == len(files) - 1:
            return "#"
    f = files[index + 1]
    return f[:-len(".jpg")] + ".html"

def get_up(index):
    return index_to_filename(int(index/image_per_page))

def get_page(index, total):
    if total == 1:
        return ""
    else:
        return "Page " + str(index + 1) + " of " + str(total)

def page(index):
    print 'handling ', files[index]
    context = {}
    if index == 0:
        context["previous"] = ""
    else:
        context["previous"] = get_previous(index)
    if index == len(files) - 1:
        context["next"] = ""
        context["next_img"] = "#"
    else:
        context["next"] = get_next(index)
        context["next_img"] = get_next_img(index)
    context["up"] = get_up(index)

    context["image"] = files[index]
    context["title"] = files[index] + " - " + page_title
    context["album"] = page_title
    context["url"] = encode(album_url + files[index][:-len(".jpg")] + ".html")
    context["analytics_id"] = config.ANALYTICS_ID

    f = open(files[index][:-len(".jpg")] + ".html", "w")
    f.write(page_html % context)
    f.close()
    
def index(index):
    context = {'title':index_title}
    total_pages = len(files) / image_per_page
    if len(files)%image_per_page > 0:
        total_pages = total_pages + 1 
    page_index = index/image_per_page
    print 'handling index page',page_index
    if page_index == 0:
        context["previous"] = ""
    else:
        context["previous"] = "<a href='" + index_to_filename(page_index - 1) + "'>Previous</a>"
    if page_index * image_per_page < len(files) - image_per_page:
        context["next"] = "<a href='" + index_to_filename(page_index + 1) + "'>Next</a>"
    else:
        context["next"] = ""

    context["page"] = get_page(page_index, total_pages)
    context["url"] = encode(album_url)
    context["analytics_id"] = config.ANALYTICS_ID
    thumbnails = []    
    for i in range(index, min(index + image_per_page, len(files))):
        thumbnails.append(thumbnail_html(i))
        page(i)
    context["thumbnails"] = "".join(thumbnails)
    out = open(index_to_filename(page_index), 'w')
    out.write(html % context)
    out.close()
    
for i in range(0, len(files), image_per_page):
    index(i)
